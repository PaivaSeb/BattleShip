﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BattleShip
{
    class ComputerPlayer : Player
    {
        int[] board = new int[100];
        int[] sf = new int[100];
        List<String> shotCoords = new List<String>();
        String difficulty;
        Boolean validShot;
        int x;
        int y;
        int lastFired = 0;
        int lastSuccess= 6;
        int lastSuccessX = -1;
        int lastSuccessY = -1;
        int lastFiredX;
        int lastFiredY;
        List<String> PlacesToShoot = new List<String>();

        public ComputerPlayer() { }

        public ComputerPlayer(String difficulty)
        {
            this.difficulty = difficulty;
        }

        public void SetDifficulty(String d)
        {
            this.difficulty = d;
        }
        //accept board
        public void SetBoard(int[] b)
        {
            board = b;
        }

        public void move()
        {
            validShot = false;
            if (this.difficulty.Equals("Not Smart"))
            {
                //Only random shots
                RandomShot();
            }
            else if (difficulty.Equals("Medium Smart"))
            {
                //randomshots until hit, then semi random
                //MessageBox.Show(!board.Contains(lastSuccess) + "  " + lastSuccess);
                if (lastFired == 0 && !board.Contains(lastSuccess))// PlacesToShoot.Count() == 0)
                {
                    PlacesToShoot.Clear();
                    RandomShot();
                }
                else
                {
                    //MessageBox.Show("WE AI IN DI SBISH");
                    //Gets all possible shots to make -- if list isnt empty
                    if (PlacesToShoot.Count != 0)
                    {
                        mediumComputerTurn();
                    }
                    else
                    {
                        for (int i = 1; i < 10; i++)
                        {
                            PlacesToShoot.Add(lastFiredX + "*" + (lastFiredY + i));
                        }
                        for (int i = 1; i < 10; i++)
                        {
                            PlacesToShoot.Add(lastFiredX + "*" + (lastFiredY - i));
                        }
                        for (int i = 1; i < 10; i++)
                        {
                            PlacesToShoot.Add((lastFiredX - i) + "*" + lastFiredY);
                        }
                        for (int i = 1; i < 10; i++)
                        {
                            PlacesToShoot.Add((lastFiredX + i) + "*" + lastFiredY);
                        }
                        //remove indexes of invalid shots
                        for (int i = PlacesToShoot.Count - 1; i >= 0; i--)
                        {
                            //MessageBox.Show("PlacesToShoot[i] " + PlacesToShoot[i] + " PlacesToShoot[i].IndexOf( * )) " + PlacesToShoot[i].IndexOf("*"));
                            int testX = Int32.Parse(PlacesToShoot[i].Substring(0, PlacesToShoot[i].IndexOf("*")));
                            int testY = Int32.Parse(PlacesToShoot[i].Substring(PlacesToShoot[i].IndexOf("*") + 1));
                            if (isOutOfBounds(testX, testY) || hasBeenShot(testX, testY))
                            {
                                PlacesToShoot.RemoveAt(i);
                            }
                        }
                        mediumComputerTurn();
                    }
                }
            }
            else
            {
                //randomshots until hit, then precise shots
                if (lastFired == 0 && !board.Contains(lastSuccess))
                {
                    PlacesToShoot.Clear();
                    RandomShot();
                }
                else
                {
                    if (PlacesToShoot.Count != 0)
                    {
                        mediumComputerTurn();
                    }
                    else
                    {
                        //Gets all possible shots to make -- if list isnt empty
                        for (int i = 1; i <= 5; i++)
                        {
                            PlacesToShoot.Add(lastFiredX + "*" + (lastFiredY + i));
                        }
                        for (int i = 1; i <= 5; i++)
                        {
                            PlacesToShoot.Add(lastFiredX + "*" + (lastFiredY - i));
                        }
                        for (int i = 1; i <= 5; i++)
                        {
                            PlacesToShoot.Add((lastFiredX - i) + "*" + lastFiredY);
                        }
                        for (int i = 1; i <= 5; i++)
                        {
                            PlacesToShoot.Add((lastFiredX + i) + "*" + lastFiredY);
                        }
                        //remove indexes of invalid shots
                        for (int i = PlacesToShoot.Count - 1; i >= 0; i--)
                        {
                            int x = Int32.Parse(PlacesToShoot[i].Substring(0, PlacesToShoot[i].IndexOf("*")));
                            int y = Int32.Parse(PlacesToShoot[i].Substring(PlacesToShoot[i].IndexOf("*") + 1));
                            if (isOutOfBounds(x, y) || hasBeenShot(x, y))
                            {
                                PlacesToShoot.RemoveAt(i);
                            }
                        }
                        hardComputerTurn();
                    }
                }
            }
        }

        //Takes first item in list and shoots
        private void hardComputerTurn()
        {
            int x = Int32.Parse(PlacesToShoot[0].Substring(0, PlacesToShoot[0].IndexOf("*")));
            int y = Int32.Parse(PlacesToShoot[0].Substring(PlacesToShoot[0].IndexOf("*") + 1));
            ValidShot(x, y);
            shotCoords.Add(x + "*" + y);
            PlacesToShoot.RemoveAt(0);
        }

        //Takes random item in list and shoots
        private void mediumComputerTurn()
        {
            Random rnd = new Random();
            int r = rnd.Next(0, PlacesToShoot.Count);
            int x = Int32.Parse(PlacesToShoot[r].Substring(0, PlacesToShoot[r].IndexOf("*"))); 
            int y = Int32.Parse(PlacesToShoot[r].Substring(PlacesToShoot[r].IndexOf("*") + 1));
            ValidShot(x, y);
            shotCoords.Add(x + "*" + y);
            PlacesToShoot.RemoveAt(r);
        }

        private void RandomShot()
        {
            Random rnd = new Random();
            x = rnd.Next(0, 10);
            y = rnd.Next(0, 10);
            Boolean isInvalidXY = false;
            while (!isInvalidXY)
            {
                for (int i = 0; i < shotCoords.Count; i++)
                {
                    if (shotCoords[i].CompareTo(x + "*" + y) == 0)
                    {
                        x = rnd.Next(0, 10);
                        y = rnd.Next(0, 10);
                        i=0;
                    }
                }
                isInvalidXY = true;
            }
            ValidShot(x, y);
            shotCoords.Add(x + "*" + y);
        }


        public Boolean hasBeenShot(int x, int y)
        {
            for (int i = 0; i < shotCoords.Count; i++)
            {
                if (shotCoords[i].CompareTo(x + "*" + y) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public Boolean isOutOfBounds(int n, int m)
        {
            return (n > 9 || m > 9 || n < 0 || m < 0);
        }


        private void ValidShot(int x, int y)
        {
            this.x = x;
            this.y = y;
            int index = x * 10 + y;
            if (board[x * 10 + y] > 0)
            {
                validShot = true;
                sf[index] = 2;
                lastSuccess = board[index];
                lastSuccessX = x;
                lastSuccessY = y;
                board[index] = 0;
            }
            else
            {
                sf[index] = 1;
            }
            lastFired = board[index];
            board[index] = 0;
            lastFiredX = x;
            lastFiredY = y;
        }

        public Boolean ShotSuccess()
        {
            return validShot;
        }

        public int GetX()
        {
            return x;
        }

        public int GetY()
        {
            return y;
        }

        public int[] getShotsFiredByComputer()
        {
            return sf;
        }
    }
}
