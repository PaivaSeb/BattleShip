﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        String diffSelect;
		Music bgMusic = new Music(@"Sounds\PlaceBoatAudio.wav");
		private Boolean Vertical = true;
		private Button button;
		private Board board;
		int[,] playerShips;
        String username;

		public Window1(String lvlSelect, String user)
        {
            username = user;
            diffSelect = lvlSelect;
            InitializeComponent();
            bgMusic.PlaybackMusic();
            bgMusic.Play();
            board = new Board(GridBoats);

		}

		private void Direction_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image img = (Image)sender;
            String name = img.Name;
            Vertical = img.Name.Substring(0, 8).Equals("Vertical");
        }

        private void PlaceBoat_MouseDown(object sender, MouseButtonEventArgs e)
        {
            button.IsEnabled = false;
            Grid grid = GridBoats;
            Image image = (Image)sender;
            Boolean placed = board.PlaceBoatInGrid(Vertical, grid, button.Name.Substring(0, (button.Name.Length - 3)), image);
            if (!placed)
            {
                MessageBox.Show("Invalid position - Please choose a new position to place your boat");
            }
            else
            {
                button.IsEnabled = false;
                grid.IsEnabled = false;
                Vertical = true;
            }
        }
		
        private void BoatBtn_Click(object sender, RoutedEventArgs e)
        {
            button = (Button)sender;
            VerticalUpImg.IsEnabled = true;
            VerticalDownImg.IsEnabled = true;
            HorizontalLeftImg.IsEnabled = true;
            HorizontalRightImg.IsEnabled = true;
            GridBoats.IsEnabled = true;
        }

        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            Board.ClearGrid(GridBoats);
            board = new Board(GridBoats);
            BattleshipBtn.IsEnabled = true;
            DestroyerBtn.IsEnabled = true;
            CarrierBtn.IsEnabled = true;
            SubmarineBtn.IsEnabled = true;
            CruiserBtn.IsEnabled = true;
            
        }

        private void RandomBtn_Click(object sender, RoutedEventArgs e)
        {
            playerShips = board.RandomPlacement(GridBoats);
            BattleshipBtn.IsEnabled = false;
            DestroyerBtn.IsEnabled = false;
            CarrierBtn.IsEnabled = false ;
            SubmarineBtn.IsEnabled = false;
            CruiserBtn.IsEnabled = false;
        }

        private void StartBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!board.ValidStart())
            {
                MessageBox.Show("Please place your boats or randomize");
            }
            else
            {
                MainWindow mainWindow = new MainWindow(diffSelect, board, username);
                mainWindow.Show();
                this.Close();
            }
        }

        private void Highscores_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
