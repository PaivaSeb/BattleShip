﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace BattleShip
{
    class HumanPlayer : Player
    {
        Boolean validShot;
        Image imgName;
        int[] randomBoats = new int[100];
        static int[] sf = new int[100];
        static List<String> shotCoords = new List<String>();
        string uname = "";
        int wins = 0;
        int loses = 0;
        
        public HumanPlayer() { }

        public HumanPlayer(object sender, int[] randomBoats, string name)
        {
            this.randomBoats = randomBoats;
            this.imgName = ((Image)sender);
            this.wins = 0;
            this.loses = 0;
            this.uname = name;

        }



        public void win()
        {
            this.wins++;
        }

        public void lose()
        {
            this.loses++;
        }

        public int Wins
        {
            get { return this.wins; }
        }

        public int Loses
        {
            get { return this.loses; }
        }


        public void move()
        {
            validShot = false;
            String name = imgName.Name;
            if(randomBoats[(name[0] - 64) * 10 - Int32.Parse(name.Substring(1))] > 0)
            {
                Console.WriteLine((name[0] - 64) * 10 - Int32.Parse(name.Substring(1)));
                sf[(name[0] - 64) * 10 - Int32.Parse(name.Substring(1))] = 2;
                Console.WriteLine(sf[(name[0] - 64) * 10 - Int32.Parse(name.Substring(1))]);
                validShot = true;
            }
            else
            {
                sf[(name[0] - 64) * 10 - Int32.Parse(name.Substring(1))] = 1;
            }
        }

        public Boolean ShotSuccess()
        {
            return this.validShot;
        }

        public int[] getShotsFiredByPlayer()
        {
            return sf;
        }

        public string Name
        {
            get { return this.uname; }
            set { this.uname = value; }
        }

        //TAKE IN THE TIMER AND NAME AS INPUT
        public void writeToFile(String levelDif)
        {
            // getting root path
            string rootLocation = typeof(MainWindow).Assembly.Location;
            // appending sound location
            String toAppend = @"HighScore\\" + levelDif + "HS.txt";
            String path = rootLocation.Replace("bin\\Debug\\BattleShip.exe", toAppend);
            fileCreation(path, levelDif);

            //path, name, time
            appendCheck(wins, loses, path);

        }

        public void fileCreation(String path, String levelDif)
        {
            // This text is added only once to the file.
            if (!File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine("Best Players VS AI(" + levelDif + ")");
                }
            }
        }

        private void appendCheck(int wins, int loses, String path)
        {
            char[] sep = { Convert.ToChar("\t") };
            String newScore = (Name + "\tWins:" + Wins + "\tLoses:" + Loses);
            String newScoreWinComparison = "Wins:" + wins.ToString();
            String newScoreComparison = newScoreWinComparison + "Loses:" + loses.ToString();
            List<String> allLines = File.ReadAllLines(path).ToList();

            //Check if theres a total of 3 highscores
            //If not add highscores
            //Still have to order these
            if (allLines.Count < 4)
            {
                //No scores yet
                if (allLines.Count == 1)
                {
                    allLines.Add(newScore);
                }


                //One highscore in
                else if (allLines.Count == 2)
                {
                    String[] splitElements = allLines[1].Split(sep);
                    //If new highscore is better
                    if (newScoreWinComparison.CompareTo(splitElements[1]) > 0)
                    {
                        allLines.Add(allLines[1]);
                        allLines[1] = newScore;
                    }
                    else if (newScoreWinComparison.CompareTo(splitElements[1]) == 0)
                    {
                        if (newScoreComparison.CompareTo(splitElements[1] + splitElements[2]) > 0)
                        {
                            allLines.Add(allLines[1]);
                            allLines[1] = newScore;
                        }
                        else
                        {
                            allLines.Add(newScore);
                        }
                    }
                    else
                    {
                        allLines.Add(newScore);
                    }
                }
                //Two highscores in
                else if (allLines.Count == 3)
                {
                    String[] splitElements = allLines[1].Split(sep);
                    String[] splitElements2 = allLines[2].Split(sep);
                    //highscore better than the first one
                    if (newScoreWinComparison.CompareTo(splitElements[1]) > 0)
                    {
                        allLines.Add(allLines[2]);
                        allLines[2] = allLines[1];
                        allLines[1] = newScore;
                    }
                    //highscore point is equal to 1st win W points
                    else if (newScoreWinComparison.CompareTo(splitElements[1]) == 0)
                    {
                        //Less L
                        if (newScoreComparison.CompareTo(splitElements[1] + splitElements[2]) > 0)
                        {
                            allLines.Add(allLines[2]);
                            allLines[2] = allLines[1];
                            allLines[1] = newScore;
                        }
                        //Equal or more L
                        else
                        {
                            allLines.Add(allLines[2]);
                            allLines[2] = newScore;
                        }
                    }
                    //More W than 2nd highscore
                    else if (newScoreWinComparison.CompareTo(splitElements2[1]) > 0)
                    {
                        allLines.Add(allLines[2]);
                        allLines[2] = newScore;
                    }
                    //Equal W to 2nd highscore
                    else if (newScoreWinComparison.CompareTo(splitElements2[1]) == 0)
                    {
                        //Less L
                        if (newScoreComparison.CompareTo(splitElements2[1] + splitElements2[2]) > 0)
                        {
                            allLines.Add(allLines[2]);
                            allLines[2] = newScore;
                        }
                        //Equal or more L
                        else
                        {
                            allLines.Add(newScore);
                        }
                    }
                    else
                    {
                        allLines.Add(newScore);
                    }
                }
                //Already 3 highscores
                else
                {


                    String[] splitElements = allLines[1].Split(sep);
                    String[] splitElements2 = allLines[2].Split(sep);
                    String[] splitElements3 = allLines[3].Split(sep);

                    if (newScoreWinComparison.CompareTo(splitElements[1]) > 0)
                    {
                        allLines[3] = allLines[2];
                        allLines[2] = allLines[1];
                        allLines[1] = newScore;
                    }
                    //highscore point is equal to 1st win W points
                    else if (newScoreWinComparison.CompareTo(splitElements[1]) == 0)
                    {
                        //Less L
                        if (newScoreComparison.CompareTo(splitElements[1] + splitElements[2]) > 0)
                        {
                            allLines[3] = allLines[2];
                            allLines[2] = allLines[1];
                            allLines[1] = newScore;
                        }
                        //Equal or more L
                        else
                        {
                            allLines[3] = allLines[2];
                            allLines[2] = newScore;
                        }
                    }
                    //More W than 2nd highscore
                    else if (newScoreWinComparison.CompareTo(splitElements2[1]) > 0)
                    {
                        allLines[3] = allLines[2];
                        allLines[2] = newScore;
                    }
                    //Equal W to 2nd highscore
                    else if (newScoreWinComparison.CompareTo(splitElements2[1]) == 0)
                    {
                        //Less L
                        if (newScoreComparison.CompareTo(splitElements2[1] + splitElements2[2]) > 0)
                        {
                            allLines[3] = allLines[2];
                            allLines[2] = newScore;
                        }
                        //Equal or more L
                        else if (newScoreComparison.CompareTo(splitElements2[1] + splitElements2[2]) > 0)
                        {
                            allLines[3] = newScore;
                        }
                    }
                    //More W than 3rd highscore
                    else if (newScoreWinComparison.CompareTo(splitElements3[1]) > 0)
                    {
                        allLines[3] = newScore;
                    }
                    else if (newScoreWinComparison.CompareTo(splitElements3[1]) == 0)
                    {
                        //Less L
                        if (newScoreComparison.CompareTo(splitElements3[1] + splitElements3[2]) > 0)
                        {
                            allLines[3] = newScore;
                        }
                    }

                }

            }
            //Convert to an array
            String[] highscores = allLines.ToArray();
            //Creates/Overwrites file at path
            File.WriteAllLines(path, highscores);
        }

    }

}
