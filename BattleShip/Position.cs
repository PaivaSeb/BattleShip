﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip
{
    class Position
    {
        int x = 0;
        int y = 0;
        public Position(String y, int x)
        {
            this.x = x;
            this.y = convertLetterToNumber(y);
        } 

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        private int convertLetterToNumber(String letter)
        {
            int y = 0;
            switch (letter)
            {
                case "A":
                    y = 0;
                    break;
                case "B":
                    y = 1;
                    break;
                case "C":
                    y = 2;
                    break;
                case "D":
                    y = 3;
                    break;
                case "E":
                    y = 4;
                    break;
                case "F":
                    y = 5;
                    break;
                case "G":
                    y = 6;
                    break;
                case "H":
                    y = 7;
                    break;
                case "I":
                    y = 8;
                    break;
                case "J":
                    y = 9;
                    break;
            }
            return y;
        }
    }
}
