﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace BattleShip
{
    class Boat
    {
        private String name;
        private int size;
		private Uri[] images;

        public Boat(String name, int size, Uri[] images)
        {
            this.name = name;
            this.size = size;
			this.images = images;
        }

        public String Name
        {
            get { return name; }
        }

        public int Size
        {
            get { return size; }
        }

		public Uri[] Images
		{
			get { return images; }
		}
    }
}
