﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Boolean playerShot = false;
        int[] playerBoats = new int[100];
		int[] computerBoats = new int[100];
		int computerCounter = 0;
		int playerCounter = 0;
        HumanPlayer p;
        ComputerPlayer cp;
        Board playerGrid;
        Boolean ended = false;
        String level;
        String diffSelect;
        private int timerTickCount = 0;
        DispatcherTimer timer;
        object senderObj;
        String Uname;
        int totalMoves = 0;
        HumanPlayer stubHM = new HumanPlayer();

        public MainWindow(String lvlSelect, Board playerGrid, String user)
        {
			InitializeComponent();
            cp = new ComputerPlayer(lvlSelect);
            createHS();
            diffSelect = lvlSelect;
            Uname = user;
            if (playerGrid == null)
			{
                string shotsComputer = @".\SaveComputerShots.xml";
                string shotsHumanPlayer = @".\SaveHumanShots.xml";
                string playerGridPath = @".\SavePlayerGrid.xml";
                string computerGridPath = @".\SaveComputerGrid.xml";
                string boats = @".\SaveBoats.xml";
                string info = @".\SaveInfo.txt";
                
                int[] boatsp = SaveReloadState.DeserializeDocument(boats);
                int [] compShots = SaveReloadState.DeserializeDocument(shotsComputer);
                int [] humanShots = SaveReloadState.DeserializeDocument(shotsHumanPlayer);
                this.playerBoats = SaveReloadState.DeserializeDocument(playerGridPath);
                this.computerBoats = SaveReloadState.DeserializeDocument(computerGridPath);
                int[,] array = Get2dArray(boatsp);
                String[] information = File.ReadAllLines(info);
                humanShots = ReverseShips(humanShots);

                this.cp.SetDifficulty(information[0]);
                this.level = information[0];
                //name = information[1];

                Grid pgrid = PlayerGrid;
                Grid cgrid = ComputerGrid;

                this.playerGrid = new Board(pgrid);
                this.playerGrid.setShips(array);
                PlaceShipsFromArray(this.playerGrid.ShipPlacement);
                
                DisplayShots(compShots, pgrid);
                DisplayShots(humanShots, cgrid);
                
                File.Delete(playerGridPath);
                File.Delete(computerGridPath);
                File.Delete(shotsComputer);
                File.Delete(shotsHumanPlayer);
                File.Delete(boats);
                File.Delete(info);               
            }
			else
			{                
                this.playerGrid = playerGrid;
                PlaceShipsFromArray(playerGrid.ShipPlacement);
                this.playerBoats = StringToIntArray(playerGrid.getShipBitMap().Replace(" ", String.Empty).Replace("\n", String.Empty), this.playerBoats);
				cp.SetDifficulty(lvlSelect);
				InitializeComponent();
				Grid grid = ComputerGrid;
                Board cgrid = new Board(grid);
                cgrid.RandomPlacement(grid);
				this.computerBoats = StringToIntArray(cgrid.getShipBitMap().Replace(" ", String.Empty).Replace("\n", String.Empty), this.computerBoats);
				Board.ClearGrid(grid);
                level = lvlSelect;
			}

		}
        private void createHS()
        {
            string rootLocation = typeof(MainWindow).Assembly.Location;
            String toAppend = @"HighScore\\" + diffSelect + "HS.txt";
            String path = rootLocation.Replace("bin\\Debug\\BattleShip.exe", toAppend);
            stubHM.fileCreation(path, diffSelect);
        }

        private int[] Convert2dArray(int[,] array)
        {
            int count = 0;
            int[] toReturn = new int[100];
            for ( int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    toReturn[count] = array[i, j];
                    count++;
                }
            }
            return toReturn;
        }

        private int[,] Get2dArray(int[] array)
        {
            int count = 0;
            int[,] toReturn = new int[10,10];
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    toReturn[i,j] = array[count];
                    count++;
                }
            }
            return toReturn;
        }

        private void PlaceShipsFromArray(int[,] shipPlacement)
        {
            Board board = new Board(PlayerGrid);
            for (int i = 0; i < shipPlacement.GetLength(0); i++)
            {
                for (int j = 0; j < shipPlacement.GetLength(1); j++)
                {
                    if (shipPlacement[i, j] != 0)
                    {
                        if (shipPlacement[i, j] % 10 == 1)
                        {
                            board.PlaceBoatInGrid(false, PlayerGrid, getBoatName(shipPlacement[i, j] / 10), (Image)Board.GetGridElement(PlayerGrid, i, j));

                        }
                        else
                        {
                            board.PlaceBoatInGrid(true, PlayerGrid, getBoatName(shipPlacement[i, j] / 10), (Image)Board.GetGridElement(PlayerGrid, i, j));
                        }
                    }
                }

            }
        }

        public void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
            if (!ended)
            {
                if (MessageBox.Show("Do you want to save current game?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    //if no
                }
                else
                {
                    //if yes
                    string playerGridPath = @".\SavePlayerGrid.xml";
                    string computerGridPath = @".\SaveComputerGrid.xml";
                    string shotsComputer = @".\SaveComputerShots.xml";
                    string shotsHumanPlayer = @".\SaveHumanShots.xml";
                    string boats = @".\SaveBoats.xml";
                    string info = @".\SaveInfo.txt";
                    
                    List<String> information = new List<String>();
                    information.Add(level);
                    //information.Add(name);
                    File.WriteAllLines(info, information.ToArray());

                    int[] compShots = cp.getShotsFiredByComputer();
                    int[] humanShots = p.getShotsFiredByPlayer();

                    int[] array = Convert2dArray(playerGrid.ShipPlacement);

                    SaveReloadState.SerializeDocument(boats, array);
                    SaveReloadState.SerializeDocument(playerGridPath, playerBoats);
                    SaveReloadState.SerializeDocument(computerGridPath, computerBoats);
                    SaveReloadState.SerializeDocument(shotsComputer, compShots);
                    SaveReloadState.SerializeDocument(shotsHumanPlayer, humanShots);
                }
            }
            else { };
		}

        public void DisplayShots(int[] shots, Grid grid)
        {
            int counter = 0;
            for ( int row = 0; row < 10; row++)
            {
               for ( int col = 0;col < 10; col++)
                {
                    Image img = (Image)Board.GetGridElement(grid, row, col);
                    switch (shots[counter])
                    {
                        case 1:
                            img.Source = new BitmapImage(new Uri("./fire.png", UriKind.Relative));
                            img.RenderTransform = new RotateTransform(0, 0.5, 0.5);
                            break;
                        case 2:
                            img.Source = new BitmapImage(new Uri("./splash2.png", UriKind.Relative));
                            img.RenderTransform = new RotateTransform(0, 0.5, 0.5);
                            break;
                    }
                    counter++;
                }
            }
        }
     
        private void AttackBtn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (firstAttack == false) {
                countDown();
                fightSequence();
                firstAttack = true;
            }
            //Human Plays
            senderObj = sender;
            totalMoves++;
            timerTickCount = 0;
            totalMoves++;
            p = new HumanPlayer(senderObj, computerBoats, Uname);
            p.move();
            if (p.ShotSuccess())
            {
                InstructionsTxt.Text = "You got a hit!";
                ((Image)sender).RenderTransform = new RotateTransform(0, 0.5, 0.5);
                ((Image)sender).Source = new BitmapImage(new Uri(uriString: "./fire.png", uriKind: UriKind.Relative));
                playerCounter++;
            }
            else
            {
                InstructionsTxt.Text = "You missed!";
                ((Image)sender).Source = new BitmapImage(new Uri(uriString: "./splash2.png", uriKind: UriKind.Relative));
                ((Image)sender).RenderTransform = new RotateTransform(0, 0.5, 0.5);
            }
            if (playerCounter == 17)
            {
                // Player wins
                p.win();
                p.writeToFile(diffSelect);
                timer.Stop();
                Victory victory = new Victory();
                victory.Owner = this;
                victory.Show();
            }


            ((Image)sender).IsEnabled = false;
            //Computer Plays
            cp.SetBoard(playerBoats); 
            cp.move();
            if (cp.ShotSuccess())
            {
                InstructionsTxt.Text = "The computer got a hit!";

                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).Source = new BitmapImage(new Uri(uriString: "./fire.png", uriKind: UriKind.Relative));
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).RenderTransform = new RotateTransform(0, 0.5, 0.5);

                computerCounter++;
            }
            else
            {
                InstructionsTxt.Text = "The computer missed!";

                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).Source = new BitmapImage(new Uri(uriString: "./splash2.png", uriKind: UriKind.Relative));
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).RenderTransform = new RotateTransform(0, 0.5, 0.5);


            }
            if (computerCounter == 17)
            {
                // Player loses
                p.lose();
                p.writeToFile(diffSelect);
                timer.Stop();
                Window2 window2 = new Window2();
                window2.Owner = this;
                window2.Show();
            }
            InstructionsTxt.Text = InstructionsTxt.Text + " It's your turn!";

        }

        Boolean firstAttack = false;
        private void AIFight()
        {
            //Computer Plays
            totalMoves++;
            HumanPlayer p = new HumanPlayer(senderObj, computerBoats, Uname);
            ((Image)senderObj).IsEnabled = false;
            cp.SetBoard(playerBoats);
            cp.move();
            if (cp.ShotSuccess())
            {
                InstructionsTxt.Text = "The computer got a hit!";

                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).Source = new BitmapImage(new Uri(uriString: "./fire.png", uriKind: UriKind.Relative));
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).RenderTransform = new RotateTransform(0, 0.5, 0.5);

                computerCounter++;
            }
            else
            {
                InstructionsTxt.Text = "The computer missed!";

                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).Source = new BitmapImage(new Uri(uriString: "./splash2.png", uriKind: UriKind.Relative));
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).RenderTransform = new RotateTransform(0, 0.5, 0.5);


            }
            if (computerCounter == 17)
            {
                // Player loses
                p.lose();
                p.writeToFile(diffSelect);
                timer.Stop();
                Window2 window2 = new Window2();
                window2.Owner = this;
                window2.Show();
            }
            InstructionsTxt.Text = InstructionsTxt.Text + " It's your turn!";
        }

        private void AIFightExtra()
        {
            HumanPlayer p = new HumanPlayer(senderObj, computerBoats, Uname);
            //Computer Plays
            cp.SetBoard(playerBoats);
            cp.move();
            if (cp.ShotSuccess())
            {
                InstructionsTxt.Text = "Oh no! You forgot to shoot and got hit!";
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).Source = new BitmapImage(new Uri(uriString: "./fire.png", uriKind: UriKind.Relative));
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).RenderTransform = new RotateTransform(0, 0.5, 0.5); computerCounter++;
            }
            else
            {
                InstructionsTxt.Text = "Luck is on your side captain, the computer missed while you weren't looking. ";
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).Source = new BitmapImage(new Uri(uriString: "./splash2.png", uriKind: UriKind.Relative));
                ((Image)Board.GetGridElement(PlayerGrid, cp.GetX(), cp.GetY())).RenderTransform = new RotateTransform(0, 0.5, 0.5);
            }
            if (computerCounter == 17)
            {
                //AddPlayerLoss
                p.lose();
                p.writeToFile(diffSelect);
                timer.Stop();
                MessageBox.Show("You lost the battle, but you haven't lost the war!");
            }
            InstructionsTxt.Text = InstructionsTxt.Text + "It's your turn!";
        }

        private void ForfeitBtn_Click(object sender, RoutedEventArgs e)
        {
            ended = true;
            MessageBox.Show("Better Luck Next Game!");
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
            Application.Current.Shutdown();
        }

        private void EnemyShipsBtn_Click(object sender, RoutedEventArgs e)
        {
            ShowShips(ReverseShips(computerBoats));
        }

        private void ShowShips(int[] randomBoats)
        {
            StringBuilder s = new StringBuilder(101);
            for (int i=1; i<=100; i++)
            {
                s.Append(randomBoats[i-1] + " ");
                if (i%10==0)
                {
                    s.Append("\n");
                }
            }
            
            MessageBox.Show(s.ToString());
        }

        private int[] ReverseShips(int[] randomBoats)
        {
            StringBuilder builder = new StringBuilder(100);
            StringBuilder s = new StringBuilder(100);
            for (int i = 1; i <= 100; i++)
            {
                s.Append(randomBoats[i - 1]);
                if (i % 10 == 0)
                {
                    for (int j = 10 - 1; j >= 0; j--)
                    {
                        builder.Append(s.ToString()[j]);
                    }
                    string newName = builder.ToString();
                    s.Clear();
                }
            }
            return StringToIntArray(builder.ToString(), new int[100]);
        }
       
        private List<int> ListBuilder()
        {
            List<int> pool = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                pool.Add(i);
            }
            return pool;
        }

        private String getBoatName(int randomKey)
        {
            String boatName;
            switch (randomKey)
            {
                case 2:
                    boatName = "Destroyer";
                    break;
                case 3:
                    boatName = "Cruiser";
                    break;
                case 4:
                    boatName = "Battleship";
                    break;
                case 5:
                    boatName = "Carrier";
                    break;
                default:
                    throw new Exception("Bad random number");
            }

            return boatName;
        }

        private int[] StringToIntArray(string b, int[] board) 
        {
            for (int i = 0; i < 100; i++)
            {
                board[i] = Int32.Parse(b.ToCharArray()[i].ToString());
            }
            return board;
        }

        private void countDown()
        {
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1); // will 'tick' once every second
            timer.Tick += new EventHandler(Timer_Tick);
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = (DispatcherTimer)sender;
            TimeLeftTxt.Text = ("Time left: " + (-(timerTickCount - 10)).ToString());
            if (++timerTickCount >= 10)
            {
                timer.Stop();
                AIFightExtra();
                timerTickCount = 0;
                timer.Start();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            countDown();
            fightSequence();
        }

        public void fightSequence()
        {
            if (totalMoves <= 200)
            {
                timerTickCount = 0;
                if (playerShot == true || timerTickCount >= 10)
                {
                    AIFight();
                }
            }
        }

        private void HS_Click(object sender, RoutedEventArgs e)
        {
            string rootLocation = typeof(MainWindow).Assembly.Location;
            String toAppend = @"HighScore\\" + diffSelect + "HS.txt";
            String path = rootLocation.Replace("bin\\Debug\\BattleShip.exe", toAppend);
            p.fileCreation(path, diffSelect);
            MessageBox.Show(File.ReadAllText(path));
        }
    }    
}
