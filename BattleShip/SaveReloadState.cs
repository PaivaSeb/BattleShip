﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BattleShip
{
	class SaveReloadState
	{
		public static void SerializeDocument(string filename, int[] array)
		{
			XmlSerializer s = new XmlSerializer(typeof(int[]));

			TextWriter myWriter = new StreamWriter(filename);

			s.Serialize(myWriter, array);

			myWriter.Close();
		}
        
        public static int[] DeserializeDocument(string filename)
		{
			int[] toReturn = null;

			XmlSerializer serializer = new XmlSerializer(typeof(int[]));

			StreamReader reader = new StreamReader(filename);

			toReturn = (int[])serializer.Deserialize(reader);

			reader.Close();

			return toReturn;
		}

	}
}
