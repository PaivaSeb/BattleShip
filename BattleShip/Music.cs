﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace BattleShip
{
	class Music
	{
		private String backgroundMusicFilePath;
		private System.Media.SoundPlayer backgroundMusicPlayer = new System.Media.SoundPlayer();

		public Music(String backgroundMusicFilePath) {
			this.backgroundMusicFilePath = backgroundMusicFilePath;
		}
            

		public void PlaybackMusic()
		{
			if (backgroundMusicFilePath != null)
			{

				// getting root path
				string rootLocation = typeof(MainWindow).Assembly.Location;
				// appending sound location
				string fullPathToSound = rootLocation.Replace("bin\\Debug\\BattleShip.exe", this.backgroundMusicFilePath);
                this.backgroundMusicPlayer.SoundLocation = fullPathToSound;
			}
		}

		public void Play() {
            this.backgroundMusicPlayer.Play();
		}

		public void end_Media() {
			this.backgroundMusicPlayer.Stop();
		}

	}
}
