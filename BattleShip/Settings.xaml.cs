﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        String username;
        public Settings()
        {
            InitializeComponent();

            string playerGridPath = @".\SavePlayerGrid.xml";

            if (File.Exists(playerGridPath))
            {
                BattleShip.MainWindow placeShips = new BattleShip.MainWindow(null, null,"Player");
                this.Close();
                placeShips.Show();
            }

        }

        private void ComboBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            
        }

        private void QuitBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void StartBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Username.Text == "")
            {
                MessageBox.Show("Username can't be empty.\nPlease enter a valid username!");
                return;
            }
            username = Username.Text;
            ///Window1CHANGETHISTOSECONDWINDOW
            BattleShip.Window1 placeShips = new BattleShip.Window1(LvlSelect.Text, username);
            this.Close();
            placeShips.Show();
        }
    }
}
