﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows;

namespace BattleShip
{
    public class Board
    {
        //GRIDS - TO SERIALIZE
        private Grid grid;
        private Boat destroyer;
        private Boat carrier;
        private Boat submarine;
        private Boat cruiser;
        private Boat battleship;
        private int[,] shipPlacement =
        {
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 }
        };

        private int[,] boatBitMap =
{
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 },
            {0,0,0,0,0,0,0,0,0,0 }
        };

        public Board(Grid grid)
        {
            this.grid = grid;
        }

        public void setShips(int[,] ships)
        {
            this.shipPlacement = ships;
        }

        public static void ClearGrid(Grid grid)
        {
            for (int i = 0; i < grid.Children.Count; i++)
            {
                Image image = (Image) grid.Children[i];
                image.Source = new BitmapImage(new Uri("white.png", UriKind.Relative));
                image.Opacity = 100;
                image.IsEnabled = true;
                image.RenderTransform = new RotateTransform(0, 0.5, 0.5);
            }
        }

        private int[,] clear2DArray(int[,] array)
        {
            int[,] cleanArray =
            {
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 },
                {0,0,0,0,0,0,0,0,0,0 }
            };
            return cleanArray;
        }

        private Boat CreateBoat(String boatName, Boolean horizontal)
        {
            BitmapImage[] images = new BitmapImage[0];
            if (horizontal)
            {
                RotateTransform rotate = new RotateTransform(90);
            }
            Uri[] uri2 = { new Uri("./2top.png", UriKind.Relative), new Uri("./2bottom.png", UriKind.Relative) };
            Uri[] uri3 = { new Uri("./3top.png", UriKind.Relative), new Uri("./3mid.png", UriKind.Relative), new Uri("./3bot.png", UriKind.Relative) };
            Uri[] uri4 = { new Uri("./4top.png", UriKind.Relative), new Uri("./4mid1.png", UriKind.Relative), new Uri("./4mid2.png", UriKind.Relative), new Uri("./4bottom.png", UriKind.Relative) };
            Uri[] uri5 = { new Uri("./5top.png", UriKind.Relative), new Uri("./5middle1.png", UriKind.Relative), new Uri("./5middle2.png", UriKind.Relative), new Uri("./5middle3.png", UriKind.Relative), new Uri("./5bottom.png", UriKind.Relative) };

            switch (boatName)
            {
                case "Carrier":
                    carrier = new Boat(boatName, 5, uri5);
                    return carrier;
                case "Battleship":
                    battleship = new Boat(boatName, 4, uri4);
                    return battleship;
                case "Cruiser":
                    cruiser = new Boat(boatName, 3, uri3);
                    return cruiser;
                case "Submarine":
                    submarine = new Boat(boatName, 3, uri3);
                    return submarine;
                case "Destroyer":
                    destroyer = new Boat(boatName, 2, uri2);
                    return destroyer;
                default:
                    return null;
            }
        }

        public Boolean PlaceBoatInGrid(Boolean vertical, Grid grid, String boatName, Image image)
        {
            int row, col;
            Boat boat = CreateBoat(boatName, vertical);

            if (grid.Children.IndexOf(image).ToString().Length == 1)
            {
                row = 0;
                col = Convert.ToInt32(grid.Children.IndexOf(image).ToString().Substring(0, 1));
            }
            else
            {
                row = Convert.ToInt32(grid.Children.IndexOf(image).ToString().Substring(0, 1));
                col = Convert.ToInt32(grid.Children.IndexOf(image).ToString().Substring(1));
            }

            int valid = VerifyPosition(boat.Images, image, grid, vertical, row, col);
            Uri[] images = boat.Images;

            if (valid == 2)
            {
                return false;
            }

            while (valid == 1)
            {
                if (!vertical)
                    col = col - 1;
                else
                    row = row - 1;
                valid = VerifyPosition(boat.Images, image, grid, vertical, row, col);
            }

            if (valid == 0)
            {
                for (int i = 0; i < images.Length; i++)
                {

                    if (!vertical)
                    {
                        if (i == 0)
                        {
                            shipPlacement[row, col] = images.Length * 10 + 1;
                        }

                        image = (Image)GetGridElement(grid, row, (col + i));
                        image.Source = new BitmapImage(images[i]);
                        image.RenderTransform = new RotateTransform(270, 0.5, 0.5);
                        image.Opacity = 100;
                        image.IsEnabled = false;
                        boatBitMap[row, col + i] = images.Length;
                    }
                    else
                    {
                        if (i == 0)
                            this.shipPlacement[row, col] = images.Length * 10 + 2;

                        image = (Image)GetGridElement(grid, (row + i), col);
                        image.Source = new BitmapImage(images[i]);
                        image.Opacity = 100;
                        image.IsEnabled = false;
                        boatBitMap[row + i, col] = (boat.Name.Equals("Submarine")? 1: images.Length);
                    }
                }
                return true;
            }
            return false;
        }
        public static UIElement GetGridElement(Grid g, int r, int c)
		{
			for (int i = 0; i < g.Children.Count; i++)
			{
				UIElement e = g.Children[i];
				if (Grid.GetRow(e) == r && Grid.GetColumn(e) == c)
					return e;
			}
			return null;
		}

        public String getShipBitMap()
        {
            //Show ships
            StringBuilder shipPlacements = new StringBuilder(100);
            for (int i = 0; i < boatBitMap.GetLength(0); i++)
            {
                for (int j = 0; j < boatBitMap.GetLength(1); j++)
                {
                    shipPlacements.Append(boatBitMap[i, j] + " ");
                }
                shipPlacements.Append("\n");
            }
            return shipPlacements.ToString();
        }

        public String Print(int[,] array)
        {
            //Show ships
            StringBuilder shipPlacements = new StringBuilder(100);
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    shipPlacements.Append(array[i, j] + " ");
                }
                shipPlacements.Append("\n");
            }
            return shipPlacements.ToString();
        }

        public int[,] ShipPlacement
        {
            get { return this.shipPlacement; }
        }

        private int VerifyPosition(Uri[] images, Image image, Grid grid, Boolean vertical, int row, int col)
        {
            for (int i = 0; i < images.Length; i++)
            {
                if (vertical)
                    image = (Image)GetGridElement(grid, (row + i), col);
                else
                    image = (Image)GetGridElement(grid, row, (col + i));

                if (image == null)
                    return 1;

                int index = image.Source.ToString().LastIndexOf('/');

				if (!image.Source.ToString().Substring(index + 1).Equals("white.png"))
					return 2;
			}
			return 0;
        }

        public Boolean ValidStart()
        {
            Boat[] boats = { destroyer, carrier, submarine, cruiser, battleship };
            foreach (Boat boat in boats)
            {
                if (boat == null)
                    return false;
            }
            return true;
        }


        public int[,] RandomPlacement(Grid grid)
        {
            Board.ClearGrid(grid);
            this.shipPlacement = clear2DArray(this.shipPlacement);
            this.boatBitMap = clear2DArray(this.boatBitMap);
            List<int> pool = ListBuilder();
            Random random = new Random();
            Image image;
            int randomKey;
            Boolean[] isVerticle = { true, false };
            for (int i = 4; i >= 0; i--)
            {
                image = (Image)Board.GetGridElement(grid, random.Next(0, 10), random.Next(0, 10));
                randomKey = pool[random.Next(0, i)];
                String boatName = getBoatName(randomKey);
                pool.Remove(randomKey);
                Boolean placed = this.PlaceBoatInGrid(isVerticle[random.Next(0, 2)], grid, boatName, image);
                while (placed == false)
                {
                    image = (Image)Board.GetGridElement(grid, random.Next(0, 10), random.Next(0, 10));
                    placed = this.PlaceBoatInGrid(isVerticle[random.Next(0, 2)], grid, boatName, image);
                }
            }
            return shipPlacement;
        }

        public static List<int> ListBuilder()
		{
			List<int> pool = new List<int>();
			for (int i = 1; i <= 5; i++)
			{
				pool.Add(i);
			}
			return pool;
		}

		public static String getBoatName(int randomKey)
		{
			String boatName;
			switch (randomKey)
			{
				case 1:
					boatName = "Carrier";
					break;
				case 2:
					boatName = "Battleship";
					break;
				case 3:
					boatName = "Cruiser";
					break;
				case 4:
					boatName = "Submarine";
					break;
				case 5:
					boatName = "Destroyer";
					break;
				default:
					throw new Exception("Bad random number");
			}
			return boatName;
		}
	}
}
