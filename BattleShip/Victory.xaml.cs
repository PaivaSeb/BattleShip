﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BattleShip
{
    /// <summary>
    /// Interaction logic for Victory.xaml
    /// </summary>
    public partial class Victory : Window
    {
        public Victory()
        {
            InitializeComponent();
        }

        private void ContinueRct_MouseEnter(object sender, MouseEventArgs e)
        {
            LinearGradientBrush myVerticalGradient = new LinearGradientBrush();
            myVerticalGradient.StartPoint = new Point(0.5, 0);
            myVerticalGradient.EndPoint = new Point(0.5, 1);
            myVerticalGradient.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF4D1809"), 0.0));
            myVerticalGradient.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFEACBBE"), 1.0));
            Continue.Fill = Continue.Fill = myVerticalGradient;
            ContinueRct.Foreground = Brushes.BurlyWood;
        }

        private void ContinueRct_MouseLeave(object sender, MouseEventArgs e)
        {
            LinearGradientBrush myVerticalGradient = new LinearGradientBrush();
            myVerticalGradient.StartPoint = new Point(0.5, 0);
            myVerticalGradient.EndPoint = new Point(0.5, 1);
            myVerticalGradient.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FF4D1809"), 0.0));
            myVerticalGradient.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFD17955"), 1.0));
            Continue.Fill = myVerticalGradient;

            LinearGradientBrush myVerticalGradient2 = new LinearGradientBrush();
            myVerticalGradient2.StartPoint = new Point(0.5, 0);
            myVerticalGradient2.EndPoint = new Point(0.5, 1);
            myVerticalGradient2.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFB8550D"), 0.0));
            myVerticalGradient2.GradientStops.Add(new GradientStop((Color)ColorConverter.ConvertFromString("#FFD4CDC7"), 1.0));

            ContinueRct.Foreground = myVerticalGradient2;

        }

        private void ContinueRct_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }
    }
}
